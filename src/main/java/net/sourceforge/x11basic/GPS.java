package net.sourceforge.x11basic;


/* GPS.java (c) 2011-2015 by Markus Hoffmann
 *
 * This file is part of X11-Basic for Android, (c) by Markus Hoffmann 1991-2015
 * ============================================================================
 * X11-Basic for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import java.util.List;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;

public class GPS implements LocationListener{
	private static final String TAG = "x11"+GPS.class.getSimpleName(); 
	private LocationManager locationManager=null;
	private String ProviderName="";

	public GPS(LocationManager lManager) {
		locationManager=lManager;    	
		if(locationManager==null) Log.e(TAG,"Error, Nullpointer Location.");
		List<String> providers = locationManager.getAllProviders();
		if(!providers.isEmpty()) {
			// Infos zu Location Providern ausgeben
			for (String name : providers) {
				LocationProvider lp = locationManager.getProvider(name);
				Log.d(TAG,
						lp.getName() + " --- isProviderEnabled(): "
								+ locationManager.isProviderEnabled(name));
				Log.d(TAG, "requiresCell(): " + lp.requiresCell());
				Log.d(TAG, "requiresNetwork(): " + lp.requiresNetwork());
				Log.d(TAG, "requiresSatellite(): " + lp.requiresSatellite());
			}	
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.NO_REQUIREMENT);
			criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);
			criteria.setAltitudeRequired(true);
			criteria.setCostAllowed(false);

			// Namen ausgeben
			ProviderName = locationManager.getBestProvider(criteria, true);
			Log.d(TAG,"Provider is "+ProviderName);
		}
	}

	public Location get_location() {
		return locationManager.getLastKnownLocation(ProviderName);      
	}
	public void start() {

		/*This is very strange: The requestLocationUpdates procedure will not return, 
		 * instead program execution is continued in native code, from where 
		 * X11basicView.get_location was called. runonuithread fixes it. */

		Log.d(TAG,"request updates from "+ProviderName);
		locationManager.requestLocationUpdates(ProviderName, 0, 0,this);
	}
	public void stop() {
		locationManager.removeUpdates(this);
	}
	@Override
	public void onStatusChanged(String provider, int status,Bundle extras) {
		Log.d(TAG, "onStatusChanged()");
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.d(TAG, "onProviderEnabled()");
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.d(TAG, "onProviderDisabled()");
	}

	@Override
	public void onLocationChanged(Location location) {
		// Koordinaten umwandeln
		double lat = location.getLatitude();
		double lng = location.getLongitude();         
		double alt = location.getAltitude();
		X11basicView.setLocation(lat,lng,alt);
		Log.d(TAG, "onLocationChanged "+lat+" "+lng);
	}
}
