package net.sourceforge.x11basic;


/* TtsM anager.java (c) 2011-2016 by Markus Hoffmann
 *
 * This file is part of X11-Basic for Android, (c) by Markus Hoffmann 1991-2015
 * ============================================================================
 * X11-Basic for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import java.util.Locale;
import android.util.Log;
import android.speech.tts.TextToSpeech;
import android.content.Context;
import android.content.Intent;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.os.Bundle;

public class TtsManager implements TextToSpeech.OnInitListener {
  private TextToSpeech tts=null;
  private Context mcontext;
  public TtsManager(Context context) {
    mcontext=context;
    if(tts==null) tts = new TextToSpeech(context, this);
    Log.d("TTS","OK: speach engine ready.");
  }
  public void shutdown() {
    if (tts != null) {
      tts.stop();
      tts.shutdown();
      tts=null;
    }
  }
  public void speak(final String text) {
    if(tts!=null) tts.speak(text, TextToSpeech.QUEUE_ADD, null);
  }
  public void setRate(final float rate) {
    if(tts!=null) tts.setSpeechRate(rate);
  }
  public void setLanguage(final int locale) {
    if(tts!=null) {
      if(locale==0) tts.setLanguage(Locale.US);
      else if(locale==1) tts.setLanguage(Locale.ENGLISH);
      else if(locale==2) tts.setLanguage(Locale.GERMAN);
      else if(locale==3) tts.setLanguage(Locale.FRENCH);
      else if(locale==4) tts.setLanguage(Locale.ITALIAN);
      else if(locale==5) tts.setLanguage(new Locale("spa"));
    }
  }
  
  public void setPitch(final float pitch) {
    if(tts!=null) tts.setPitch(pitch);
  }

  public void onInit(int status) {
    Log.d("TTS", "Initilization ...");
    if (status == TextToSpeech.SUCCESS) {
      try {  /*Um einen seltenen Fehler zu umgehen...*/
        int result = tts.setLanguage(Locale.US);
        if (result == TextToSpeech.LANG_MISSING_DATA
            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
          Log.e("TTS", "This Language is not supported");
          // missing data, install it
          Intent installIntent = new Intent();
          installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
          mcontext.startActivity(installIntent);
          Log.e("TTS", "This Language need to be installed");
        } else {
          Log.e("TTS", "Initilization successfull!");
        }
      } catch(NullPointerException e) {
        Log.e("TTS", "Nullpointer Exception after init.");
      }
    } else {
      Log.e("TTS", "Initilization Failed! speach engine not available.");
    }
  }
}
