package net.sourceforge.x11basic;

/* X11BasicView.java (c) 2011-2015 by Markus Hoffmann
 *
 * This file is part of X11-Basic for Android, (c) by Markus Hoffmann 1991-2015
 * ============================================================================
 * X11-Basic for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;
import android.preference.PreferenceManager;

// public class X11basicView extends View  {
public class X11basicView extends EditText implements TextWatcher {
	public static Bitmap mBitmap=null;
	private long mStartTime;
	static X11BasicActivity god;
	
	private int sx,sy,sw,sh; /* Dimension dieses Views, max. Zeichenbereich....*/
	static boolean mredrawnec=true;
	private int focus=3; /* Screen focus */
	private int focusmouse=0;  /* Mousekoordinate fuer focus */
	private int focuscursor=0;  /* Cursorkoordinate fuer focus */

	/* implementend by libx11basic.so */
	static native void InitScreen(Bitmap  bitmap);
	private static native void Init(long time_ms, String commands);
	static native void Load(String filename);
	static native void Run(); 
	static native void New();
	static native void StopCont(); 
	static native void Stop(); 
	static native int Batch();  
	static native void Programmlauf(); 
	static native void Putchar(int in); 
	static native void terminalfontsize(int fnt); 
	static native void SetMouse(int x,int y, int k); 
	static native void SetMouseMotion(int x,int y); 
	static native void SetKeyPressEvent(int kk,int a,int hi);
	static native void SetKeyReleaseEvent(int kk,int a,int hi);
	static native void Stdin(String in);  
	static native String Stdout(); 
	static native String Getcrashinfo();
	static native void setObject(X11basicView a);
	static native void queueKeyEvent(int key, int state); 
	static native int Compile(String ofilename); 
	static native void setSensorValues(int offset,int nval, float a0, float a1,float a2);
	static native void setHomeDir(String dir);
	static native void setLocation(double lat, double lon, double alt);
	static native void setLocationInfo(float bar, float acu, float speed, long time, String prov);
	static native void AudioFillStreamBuffer(short[] streamBuffer, int bufferSize);
	private static final String TAG = X11basicView.class.getSimpleName(); 
	static native void Loadandrun(String filename);
	static native int getFocuscursor();


	protected void initX11basicView() {
		// setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		setFocusable(true); //necessary for getting the touch events
		setFocusableInTouchMode(true);
		addTextChangedListener(this); 
		SharedPreferences settings =PreferenceManager.getDefaultSharedPreferences(god);
		boolean do_splash=settings.getBoolean("prefs_splash", true);
		if(do_splash) Init(System.currentTimeMillis() - mStartTime,"x11basic");
		else Init(System.currentTimeMillis() - mStartTime,"x11basic -q");
		Log.d(TAG,"Init X11Basic View"); 
		final int W = 640;
		final int H = 400; 
                sx=0;
		sy=0;
		sw=W;
		sh=H;
		if(mBitmap==null) { /*Ansonsten gibt es noch einen alten Screen, dessen groesse sich ggf. mit onScreensizechanged aendert.*/
			mBitmap = Bitmap.createBitmap(W, H, Bitmap.Config.RGB_565);
			mStartTime = System.currentTimeMillis();
			InitScreen(mBitmap);
			Log.d(TAG,"Initscreen/Defaultbitmap640x400 done."); 
		}
	}
	public void setfontsize(final int fnt) {
		Log.d(TAG,"Set fontsize");
		terminalfontsize(fnt);
		InitScreen(mBitmap);
		redraw();
	}
	public void setfocus(final int foc) {
		Log.d(TAG,"Set focus");
		focus=foc;
		redraw();
	}
	public X11basicView(Context context) {
		super(context);
		Log.d(TAG,"X11Basic View Constructor");
		god=(X11BasicActivity) context;
		initX11basicView();

	}
	public X11basicView(Context context, AttributeSet attrs) {
		super(context, attrs);
		Log.d(TAG,"X11Basic View Constructor");
		god=(X11BasicActivity) context; 
		initX11basicView(); 

	}
	public X11basicView(Context context, AttributeSet attrs, int defaultStyle) {
		super(context, attrs, defaultStyle);
		Log.d(TAG,"X11Basic View Constructor");
		god=(X11BasicActivity) context;  
		initX11basicView(); 
	}   
	
	void putchar2(byte a) {
		// check if background-Task is still alive
		if(god.mBackgroundTask!=null && god.mBackgroundTask.isAlive()) Putchar(a);
		else god.toaster("Background task is not alive!");
	}
	void putchar2(char a) {
		// check if background-Task is still alive
		if(god.mBackgroundTask!=null && god.mBackgroundTask.isAlive()) {
			char[] aa=new char[1];
			aa[0]=a;
			String b=new String(aa);
			byte[] bb=b.getBytes();
			for(int i=0;i<bb.length;i++) Putchar(bb[i]);
		} else god.toaster("Background task is not alive!");
	}
	void putchar2(String a) {
		// check if background-Task is still alive
		if(god.mBackgroundTask!=null && god.mBackgroundTask.isAlive()) {
			byte[] bb=a.getBytes();
			for(int i=0;i<bb.length;i++) Putchar(bb[i]);
		} else god.toaster("Background task is not alive!");
	}

	
	/* Konfiguriere das Keyboard, insbes. dass es in landscape nicht den ganzen 
	 * Screen ausfuellt.*/
	@Override
	public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
		// To support multiple IME
		outAttrs.imeOptions = EditorInfo.IME_FLAG_NO_EXTRACT_UI;
		outAttrs.inputType = InputType.TYPE_CLASS_TEXT;
		/*
		 * Aus der Documentation fuer InputType.TYPE_NULL: 
		 * "This should be interpreted to mean that the target input connection is not rich, 
		 * it can not process and show things like candidate text nor retrieve the current text, 
		 * so the input method will need to run in a limited 'generate key events' mode."
		 */      
		// outAttrs.inputType = InputType.TYPE_NULL;
		//
		InputConnection ic = new BaseInputConnection(this,false);
		/* The false second argument puts the BaseInputConnection into "dummy" mode, which is also required in 
		 * order for the raw key events to be sent to your view. In the BaseInputConnection code, you can find several 
		 * comments such as the following: "only if dummy mode, a key event is sent for the new text and the current 
		 * editable buffer cleared."
		 */
		return ic;
	}

	@Override
	public boolean onCheckIsTextEditor() {
		Log.d(TAG, "onCheckIsTextEditor");
		return true;
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_MENU) {
			Log.i("KEY", "menu");  
			return super.onKeyDown(keyCode, event);	
		} else if(keyCode == KeyEvent.KEYCODE_HOME) {
			Log.i("Key", "home");  
			return super.onKeyDown(keyCode, event);
		} else if(keyCode == KeyEvent.KEYCODE_BACK) {
			Log.d("Key", "back");
			/* Wenn noch ein Programm läuft, dann programm beenden, 
			 * wenn keins mehr läuft, standard aktion (super....)
			 * */
			if(Batch()==0) {

				/* Brutales Ende.*/
				int pid = android.os.Process.myPid();
				android.os.Process.killProcess(pid); 
				/*Dies führte zu seltsamen suspend zustaenden der App...*/
				return super.onKeyDown(keyCode, event);
			}
			else {
				Stop(); 
				return true;
			}
		} else if(keyCode == KeyEvent.KEYCODE_DEL) {
			Log.i("Key", "backspace");
			SetKeyPressEvent(keyCode,8,0xff);
			putchar2((byte)8);
			return true;
		} else if(keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
			putchar2((byte)27); write("[D");SetKeyPressEvent(keyCode,0x51,0xff);
			return true;
		} else if(keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
			putchar2((byte)27); write("[C");SetKeyPressEvent(keyCode,0x53,0xff);
			return true;
		} else if(keyCode == KeyEvent.KEYCODE_DPAD_UP) {
			putchar2((byte)27); write("[A");SetKeyPressEvent(keyCode,0x52,0xff);
			return true;
		} else if(keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
			putchar2((byte)27); write("[B");SetKeyPressEvent(keyCode,0x54,0xff);
			return true;
		} else {
			int a=event.getUnicodeChar();
			char c=(char)a;
			int hi=0;
			if(a==13 || a==10 || a==8 || a==9 || a==27) hi=0xff;
			SetKeyPressEvent(keyCode,a,hi);
			if(a!=0) {
				putchar2(c); 
				return true; 
			} else Log.d(TAG,"Key "+keyCode+" "+event.getUnicodeChar());
		}  
		return false; 
	}   
	@Override 
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_MENU) {
			return super.onKeyUp(keyCode, event);	
		} else if(keyCode == KeyEvent.KEYCODE_HOME) {
			return super.onKeyUp(keyCode, event);
		}//	super.onKeyUp(keyCode, event);
		//	queueKeyEvent(keyCode, 0);
		int a=event.getUnicodeChar();
		if(a!=0) SetKeyReleaseEvent(keyCode,a,0);
		else SetKeyReleaseEvent(keyCode,a,0xff);
		return false;
	}
	@Override
	public boolean onTrackballEvent (MotionEvent event) {
		int dx=(int) event.getX();
		int dy=(int) event.getY();
		SetMouseMotion(dx,dy);
		Log.d(TAG,"Trackball ");
		return super.onTrackballEvent(event); 
	} 
	@Override 
	public boolean onTouchEvent (MotionEvent event) {
		int x=(int) event.getX();
		int y=(int) event.getY();
		int k=event.getAction();  
		SetMouse(x,y,k);
		focusmouse=y;
		Log.d(TAG,"Mouseklick "+x+" "+y+" "+" "+event.getAction());
		// performClick();
		return true; // damit nicht noch das Kontextmenu aufgeht...
		// return super.onTouchEvent(event); 
	} 
	//@Override 
	//public boolean performClick() {
		
	//	return true; // damit nicht noch das Kontextmenu aufgeht...
	//}
	public void write(final String s) {  
		if(s.length()>0) putchar2(s); 
	}


	private void redraw() {
		// Log.d(TAG,"*** redraw. ***");
		mredrawnec=true;
		//	 this.postInvalidate();
	} 

	protected MySensorEventListener s1Listener=null;
	protected MySensorEventListener s2Listener=null;
	protected MySensorEventListener s3Listener=null;
	protected MySensorEventListener s4Listener=null;
	protected MySensorEventListener s5Listener=null;
	protected MySensorEventListener s6Listener=null;
	protected MySensorEventListener s7Listener=null;
	protected MySensorEventListener s8Listener=null;
	protected MySensorEventListener s9Listener=null;
	protected MySensorEventListener s10Listener=null;
	protected MySensorEventListener s11Listener=null;
	protected MySensorEventListener s12Listener=null;

	private void sensor_onoff(final int onoff) {
		Log.d(TAG,"sensoronoff: "+onoff);
		if(onoff==1) { 
			s1Listener= new MySensorEventListener(0,1);
			X11BasicActivity.sManager.registerListener(s1Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_TEMPERATURE), SensorManager.SENSOR_DELAY_NORMAL);
			s7Listener= new MySensorEventListener(1,1);
			X11BasicActivity.sManager.registerListener(s7Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_PRESSURE), SensorManager.SENSOR_DELAY_NORMAL);
			s2Listener= new MySensorEventListener(2,1);
			X11BasicActivity.sManager.registerListener(s2Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_LIGHT), SensorManager.SENSOR_DELAY_NORMAL);
			s3Listener= new MySensorEventListener(3,1);
			X11BasicActivity.sManager.registerListener(s3Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_PROXIMITY), SensorManager.SENSOR_DELAY_NORMAL);
			s4Listener= new MySensorEventListener(4,3);
			X11BasicActivity.sManager.registerListener(s4Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_NORMAL);
			s5Listener= new MySensorEventListener(7,3);
			X11BasicActivity.sManager.registerListener(s5Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_NORMAL);
			s6Listener= new MySensorEventListener(10,3);
			X11BasicActivity.sManager.registerListener(s6Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_NORMAL);
			s7Listener= new MySensorEventListener(13,3);
			X11BasicActivity.sManager.registerListener(s7Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
			s8Listener= new MySensorEventListener(16,1);
			X11BasicActivity.sManager.registerListener(s8Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE), SensorManager.SENSOR_DELAY_NORMAL);
			s9Listener= new MySensorEventListener(17,1);
			X11BasicActivity.sManager.registerListener(s9Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY), SensorManager.SENSOR_DELAY_NORMAL);
			s10Listener= new MySensorEventListener(18,3);
			X11BasicActivity.sManager.registerListener(s10Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_NORMAL);
			s11Listener= new MySensorEventListener(21,3);
			X11BasicActivity.sManager.registerListener(s11Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_GRAVITY), SensorManager.SENSOR_DELAY_NORMAL);
			s12Listener= new MySensorEventListener(24,3);
			X11BasicActivity.sManager.registerListener(s12Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION), SensorManager.SENSOR_DELAY_NORMAL);
/*
	        X11BasicActivity.sManager.registerListener(s11Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION), SensorManager.SENSOR_DELAY_NORMAL);
	        X11BasicActivity.sManager.registerListener(s11Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR), SensorManager.SENSOR_DELAY_NORMAL);
	        X11BasicActivity.sManager.registerListener(s11Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED), SensorManager.SENSOR_DELAY_NORMAL);
	        X11BasicActivity.sManager.registerListener(s11Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE_UNCALIBRATED), SensorManager.SENSOR_DELAY_NORMAL);
	        X11BasicActivity.sManager.registerListener(s11Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER), SensorManager.SENSOR_DELAY_NORMAL);
	        X11BasicActivity.sManager.registerListener(s11Listener, X11BasicActivity.sManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR), SensorManager.SENSOR_DELAY_NORMAL);
*/

		} else {
			if(s1Listener!=null)  {X11BasicActivity.sManager.unregisterListener(s1Listener);s1Listener=null;} 
			if(s2Listener!=null)  {X11BasicActivity.sManager.unregisterListener(s2Listener);s2Listener=null;} 
			if(s3Listener!=null)  {X11BasicActivity.sManager.unregisterListener(s3Listener);s3Listener=null;} 
			if(s4Listener!=null)  {X11BasicActivity.sManager.unregisterListener(s4Listener);s4Listener=null;} 
			if(s5Listener!=null)  {X11BasicActivity.sManager.unregisterListener(s5Listener);s5Listener=null;} 
			if(s6Listener!=null)  {X11BasicActivity.sManager.unregisterListener(s6Listener);s6Listener=null;}
			if(s7Listener!=null)  {X11BasicActivity.sManager.unregisterListener(s7Listener);s7Listener=null;} 
			if(s8Listener!=null)  {X11BasicActivity.sManager.unregisterListener(s8Listener);s8Listener=null;} 
			if(s9Listener!=null)  {X11BasicActivity.sManager.unregisterListener(s9Listener);s9Listener=null;} 
			if(s10Listener!=null) {X11BasicActivity.sManager.unregisterListener(s10Listener);s10Listener=null;} 
			if(s11Listener!=null) {X11BasicActivity.sManager.unregisterListener(s11Listener);s11Listener=null;} 
			if(s12Listener!=null) {X11BasicActivity.sManager.unregisterListener(s12Listener);s12Listener=null;} 
		}
	}

	@Override
	protected  void onDraw(Canvas canvas) {
	        if(mBitmap!=null) {
		  if(focus==1)      canvas.drawBitmap(mBitmap, 0, 0, null); /* Zeige immer oberen Teil*/
		  else if(focus==2) canvas.drawBitmap(mBitmap, 0, -sy, null); /* Zeige immer unteren Teil*/
		  else if(focus==4) {
		    int v=getFocuscursor()-(sh-sy)/2;
		    if(v<0) v=0;
		    if(v>sy) v=sy;
		    canvas.drawBitmap(mBitmap, 0, -v, null); /* Zeige immer Teil d. Screen mit Cursor*/
		  } else if(focus==5) {
		    int v=focusmouse-(sh-sy)/2;
		    if(v<0) v=0;
		    if(v>sy) v=sy;
		    canvas.drawBitmap(mBitmap, 0, -v, null); /* Zeige immer Teil d. Screen mit Maus*/
		  }
                  else canvas.drawBitmap(mBitmap, new Rect(0,0,sw,sh), new Rect(0,0,sw,sh-sy), null); /* Zeige immer verkleinerten Screen*/
                }
		// mredrawnec=false;
		// force a redraw after 100ms
		X11BasicActivity._redrawHandler.sleep(50);
	} 
	@Override   
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	        int w,h;
		final int modex=MeasureSpec.getMode(widthMeasureSpec);
		final int sizex=MeasureSpec.getSize(widthMeasureSpec); 
		final int modey=MeasureSpec.getMode(heightMeasureSpec);
		final int sizey=MeasureSpec.getSize(heightMeasureSpec);
		if (modex == MeasureSpec.UNSPECIFIED) w=sw;
		else w=sizex;
		if (modey == MeasureSpec.UNSPECIFIED) h=sh; 
		else h=sizey;
		if(w<=0) w=32;
		if(h<=0) h=32;

		setMeasuredDimension(w, h);

		Log.d(TAG,"Measure/screen ("+ w + "," + h + ")");
	}
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		Log.d(TAG, "onSizeChanged (" + w + "," + h + ")("+oldw+ "," + oldh + ")");
		//if (oldw == 0) {  /*  Fange nur den ersten Change ab, naemlich wenn der View zum erstenmal geoeffent wird, 
		//                      und nicht auch noch, wenn er jeweils vom Keyboard o.ae. verkleinert wird. */
                if(w==oldw) { /*dann hat sich nur die Hoehe veraendert, z.B. wenn das Keyboard erscheint oder 
		              verschwindet. */
		   if(h>oldh) { /* Dann vergroessere den Artbeitsbereich....*/
		     setScreenSize(w, h);
		   }
		   if(sh>h) sy=sh-h;
		   else sy=0;
		} else if(w!=oldw && w>=32) { /*Das heisst, erstes mal oeffnen, oder rotation*/
			setScreenSize(w, h);
			sx=0;
			sy=0;
		} 
	}

	public void setScreenSize(final int w, final int h) {
		Log.d(TAG, "setScreenSize (" + w + "," + h + ")");
		/* Copy min Portion of the old screen to new sized screen*/
		final Bitmap oldbmp=mBitmap;
		mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
		if(oldbmp!=null) {
			Canvas wideBmpCanvas = new Canvas(mBitmap); 
			Rect src = new Rect(0, 0, (oldbmp.getWidth()<mBitmap.getWidth()?oldbmp.getWidth():mBitmap.getWidth()), 
					(oldbmp.getHeight()<mBitmap.getHeight()?oldbmp.getHeight():mBitmap.getHeight()));
			Rect dest = new Rect(src); 
			dest.offset(0, 0);
			wideBmpCanvas.drawBitmap(oldbmp, src, dest, null); 
		}
		sw=w;
		sh=h;
		InitScreen(mBitmap);
		// this.postInvalidate();
	}



	boolean mf=false;
	@Override
	public void  onTextChanged(CharSequence s, int start, int before, int count){
		final int l=s.length();
		char a;
		int hi;
		Log.d(TAG, "on ts <"+s+"> "+start+" "+before+" "+count); 
		if(l>start && mf==false) {
			for(int i=start;i<start+count;i++) {
				a=s.charAt(i);
				Log.d(TAG,"Char: "+a);
				putchar2(a);
				if(a==9 || a==27 || a==13 || a==10) hi=0xff;
				else hi=0;
				SetKeyPressEvent(a,a,hi);
				SetKeyReleaseEvent(a,a,hi);
			}
			mf=true;
		} 
	}
	@Override
	public void afterTextChanged(Editable s) {
		Log.d(TAG, "after ts: " +s); 
		mf=false;
	}
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,int after) {
		int i;

		if(count>0) {
			for(i=0;i<count;i++) {
				//             Log.d(TAG,"Char: BSP");
				putchar2((byte)8);
				SetKeyPressEvent(8,8,0xff);
				SetKeyReleaseEvent(8,8,0xff);
			}
		}
		//		removeTextChangedListener(this);
		//		setText("");
		Log.d(TAG, "before ts: "+s + " "+start+" "+count+" "+after); 
		//		addTextChangedListener(this);
		//		mf=false; 
	}
	class MySensorEventListener implements SensorEventListener {
		private int offset,nval;
		MySensorEventListener(int n1,int n2) {
			offset=n1;
			nval=n2;
		}
		@Override
		public void onSensorChanged(SensorEvent event) {
			Log.d(TAG,"Sensor "+offset+" : " + event.values[0]
					+" "+event.values[1]+" "+event.values[2]+" > "+event.values.length);
			if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
				Log.d(TAG, "Sensor unreliable ("+event.accuracy+"), "  + event.sensor.getType());
			}
			setSensorValues(offset,nval, event.values[0],event.values[1],event.values[2]);
		}
		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// TODO Auto-generated method stub

		}
	}

	/******************  
	 * Sound funktionen 
	 * ********************/
	private AudioTrack mMusicTrack=null;
	private boolean mStopAudioThreads;
	Thread mStreamThread;
	Runnable mStreams = new Runnable() { 
		public void run() {
			// Create a streaming AudioTrack for music playback  
			final int minBufferSize = AudioTrack.getMinBufferSize(22050, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
			final int bufferSize = minBufferSize; 
			int writechunck=512;
			if(writechunck>minBufferSize) writechunck=minBufferSize;

			final short[] streamBuffer = new short[bufferSize]; 
			mMusicTrack = new AudioTrack(AudioManager.STREAM_MUSIC, 22050, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize, AudioTrack.MODE_STREAM);
			mMusicTrack.play();  
			Log.d(TAG,"audio buffer="+bufferSize);
			while (!mStopAudioThreads) {  
				AudioFillStreamBuffer(streamBuffer, writechunck);// Fill buffer with PCM data from C
				mMusicTrack.write(streamBuffer, 0, writechunck);// Stream PCM data into the music AudioTrack
			}
			mMusicTrack.flush();
			mMusicTrack.stop();
		}
	};

	/* Sollte von C aufgerufen werden (sound_init) */
	void RunAudioThreads() {
		mStopAudioThreads = false;

		//   mSampleThread = new Thread(mSamples);
		//   mSampleThread.start();

		mStreamThread = new Thread(mStreams);
		mStreamThread.start();
	}  

	/*kann von activity bei OnPause aufgerufen werden, fuer resume haben wir noch
	nix. hier muesste man sich merken, ob schon initialisiert war.*/

	public void StopAudioThreads() {
		mStopAudioThreads = true;
		try {
			mStreamThread.join();
		} catch(final Exception ex) {ex.printStackTrace();}
	}

	double sample[];
	byte generatedSnd[];
	final int sampleRate = 8000;
	void genTone(final int numSamples,final float freqOfTone){
		// fill out the array
		for (int i = 0; i < numSamples; ++i) {
			sample[i] = Math.sin(2 * Math.PI * i / (sampleRate/freqOfTone));
		}
		// convert to 16 bit pcm sound array
		// assumes the sample buffer is normalised.
		int idx = 0;
		for (final double dVal : sample) {
			// scale to maximum amplitude
			final short val = (short) ((dVal * 32767));
			// in 16 bit wav PCM, first byte is the low order byte
			generatedSnd[idx++] = (byte) (val & 0x00ff);
			generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
		}
	}
	void playSound(){
		final AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
				sampleRate, AudioFormat.CHANNEL_OUT_MONO,
				AudioFormat.ENCODING_PCM_16BIT, generatedSnd.length,
				AudioTrack.MODE_STATIC);
		audioTrack.write(generatedSnd, 0, generatedSnd.length);
		audioTrack.setLoopPoints(0,generatedSnd.length-2 , -1);
		audioTrack.play();

	}
	private void playtone(final float freq,final float volume,final int length) {
		final int numSamples = length * sampleRate/1000;
		sample = new double[numSamples];
		generatedSnd = new byte[2 * numSamples];
		genTone(numSamples,freq);
		playSound();
		//	new ToneGenerator(AudioManager.STREAM_MUSIC, (int)((float)ToneGenerator.MAX_VOLUME*volume)).startTone(ToneGenerator.TONE_DTMF_4, length);
	}

	private void speek(final String text,final float pitch,final float rate, final int locale) {
		Log.d(TAG,"speek angekommen p="+pitch+" r="+rate+" "+text); 
		god.speek(text,pitch,rate,locale);
	} 
	private void beep() {
		Log.d(TAG,"beep "); 
		god.beep(); 
	}
	private void showk() {
		Log.d(TAG,"showk "); 
		god.showk(); 
	}
	private void hidek() {
		Log.d(TAG,"hidek "); 
		god.hidek(); 
	}
	private void playsoundfile(final String name) {
		Log.d(TAG,"playsoundfile "+name); 
		god.playsoundfile(name);
	}

	private void call_intent(final String action,final String data,final String extra) {
		Log.d(TAG,"call intent angekommen "+action+" r="+data+" "+extra); 
		god.call_intent(action,data,extra);
	} 
	/******************
	 * GPS Funktionen 
	 * ********************/
	private GPS gps=null;

	private void gps_onoff(final int onoff) {
		if(onoff==1) {
			get_location();
			if(gps!=null) {
			  god.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					gps.start();
				}
			  });
			  //gps.start();
			}
		} else {
			if(gps!=null) {
				gps.stop();
				gps=null;
			}
		}
	}
	private void get_location() {
		if(gps==null) gps=new GPS(X11BasicActivity.lManager);
		Location location=null;
		if(gps!=null) location= gps.get_location();
		if (location != null) { 
			final double lat = location.getLatitude();
			final double lng = location.getLongitude();
			final double alt = location.getAltitude(); 

			final float bearing=location.getBearing();
			final float Accuracy = location.getAccuracy();
			final float Speed = location.getSpeed();
			final String Provider = location.getProvider();
			final long Time = location.getTime();
			Log.d(TAG,"Latitude="+lat+", Longitude="+lng+" Altitude="+alt);
			Log.d(TAG,"b="+bearing+", a="+Accuracy+" s="+Speed);
			setLocation(lat,lng,alt);
			setLocationInfo(bearing,Accuracy,Speed,Time,Provider);
		} else { 
			setLocation(-1,-1,0);
			setLocationInfo(-1,-1,-1,0,"");
			Log.d(TAG,"No Location");
		} 
	}

	void nativeCrashed(final int sig) {
		god.nativeCrashed(sig,"stdout=<"+Stdout()+">\n"+Getcrashinfo());
	}
}
