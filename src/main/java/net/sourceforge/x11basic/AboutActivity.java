package net.sourceforge.x11basic;

/* AboutActivity.java (c) 2011-2015 by Markus Hoffmann
 *
 * This file is part of X11-Basic for Android, (c) by Markus Hoffmann 1991-2015
 * ============================================================================
 * X11-Basic for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 
 

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;
  
public class AboutActivity extends Activity { 
    private TextView readme;  
   
    @Override  
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);
         
        readme=(TextView)findViewById(R.id.description);
        readme.setText(Html.fromHtml(getResources().getString(R.string.readme)+
        		getResources().getString(R.string.news)+getResources().getString(R.string.impressum)
        		));   
    }
}
