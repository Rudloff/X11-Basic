package net.sourceforge.x11basic;

/* X11BasicActivity.java (c) 2011-2015 by Markus Hoffmann
 *
 * This file is part of X11-Basic for Android, (c) by Markus Hoffmann 1991-2015
 * ============================================================================
 * X11-Basic for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream; 
import java.io.OutputStreamWriter;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Locale;
 
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.ClipboardManager;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.ViewGroup;
import android.content.DialogInterface.OnShowListener;

public class X11BasicActivity extends Activity {
	private static final String TAG = "X11Basic";
	private static final String RELINFO = "prefs_relinfo_6";
	private static String[] mFileList;
	private static boolean SDcardusable;
	private static File Basestoragedirectory;
	private boolean[] mcheckitems; 
	public RunThread mBackgroundTask = null;
	private static String mChosenFile="new.bas";
	private static String mSelectedFile="";
	final Handler h=new Handler();
	private static boolean misloaded=false;
	private static boolean sessionretained=false;
	static SensorManager		sManager;
	static LocationManager		lManager;
	static TtsManager		tManager;
	static X11basicView screen; 
	static int mfontsize=0;    /* Schriftgroesse  default=0 --> auto*/
	static int mfocus=3;       /* Screen focus    default=3 --> scale*/
	static Bitmap aBitmap;
	static Item[] items; 
	private AlertDialog mad;
	public int MY_TTSDATA_CHECK_CODE = 6014;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharedPreferences settings =PreferenceManager.getDefaultSharedPreferences(this);
		boolean do_title=settings.getBoolean("prefs_header", true);
		boolean do_status=settings.getBoolean("prefs_status", true);
		boolean do_keyboard=settings.getBoolean("prefs_keyboard", true);
		boolean shown_releaseinfo=settings.getBoolean(RELINFO, false);
		if(!shown_releaseinfo) {
		  SharedPreferences.Editor editor = settings.edit();
		  editor.putBoolean(RELINFO, true);
		  showDialog(0);
		  editor.commit();
		}
		
		// if(do_title) ;
		// else requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);
		if(do_status) {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
		} else {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}

		
		screen= (X11basicView) this.findViewById(R.id.screen);
		X11basicView.setObject(screen);

		//  screen.setInputType(InputType.TYPE_NULL);
		screen.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				// screen.setInputType(InputType.TYPE_CLASS_TEXT);
				screen.onTouchEvent(event); // call native handler
				return true; // consume touch even
			}  
		});
		setfontsize(Integer.valueOf(settings.getString("prefs_fontsize", "0")));
		setfocus(Integer.valueOf(settings.getString("prefs_focus", "3")));

		if(!do_keyboard) getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		
		/*Das hier ist um die Hardwarebeschleunigung fuer den X11Basic View auf jeden Fall
		 * abzuschalten. (Scheint in der Android 2.2. Target Version nicht zu gehen. Also muss der
		 * User das in den System-Settings abschalten (in den Entwickler-Einstellungen Hardwareoverlays 
		 * deaktivieren AN und Gpu erzwingen AUS) oder wir verlieren die Lauffaehigkeit fuer die alten
		 * Androids.) */

		// if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
		//     screen.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		// }

		// Machen wir spaeter mal. Sonst ist mousefunktion beeintraechtigt....
		// registerForContextMenu(screen);
		if (isDevice()) {
			sManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
			lManager = (LocationManager) getSystemService(LOCATION_SERVICE);
			tManager = new TtsManager(this);
		} else {
			sManager = null;
			lManager = null;
			tManager = null;
		}

		// There are two situations where onCreate is called: (a) a completely new activity;
		// (b) a new activity created as part of a change in orientation.
		// We can tell the difference by looking at the result of getLastNonConfigurationInstance.
		Object retained = getLastNonConfigurationInstance();
		if (retained == null) {
			// If there is no retained object, no Background Thread is in Progress
			/* Hier dürfen wir davon ausgehen, dass die App zum ersten mal gestartet wird, und nicht noch 
			   im Hintergrund läuft oder nur Wiedergestartet wurde. 
			   
			   Das heisst, wir sollten hier 
			   1. die assets kopieren
			   2. Evtl. Shortcuts anlegen
			   3. Evtl. programm automatisch starten, wenn über Shortcut gestartet wurde
			   4. Autostart aus Assets relisieren.
			   
			   5. TTS engine aktivieren.
			   
			   */
			String autoruninfcontent=null;
			String autorunfile="autorun.bas";
			Boolean donocopy=false;
			// open autorun.inf
			AssetManager assetManager = getAssets();
			try {
				Log.d(TAG,"read autorun.inf");
				InputStream is=assetManager.open("autorun.inf");
				int size = is.available();
				byte[] buffer = new byte[size];
				is.read(buffer);
				is.close();

				// byte buffer into a string
				autoruninfcontent = new String(buffer);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(autoruninfcontent!=null) {
				String lines[]=autoruninfcontent.split("\n");
				// read first line
				if(lines.length>0) autorunfile=lines[0];
				if(lines.length>1) donocopy=lines[1].equalsIgnoreCase("nocopy");
				Log.d(TAG,"autorunfile="+autorunfile);
				Log.d(TAG,"donocopy="+donocopy);
			}
			/* Check if the SD-Card is usable. If not, display an error and try to use the internal 
			 * Storage. */
			SDcardusable=Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
			if(SDcardusable) {
				Basestoragedirectory=Environment.getExternalStorageDirectory();
			} else {
				Basestoragedirectory=getFilesDir(); 
				showDialog(7);
			}
			if(donocopy==false) {
				asset_copy("bas"); /*copy the example programs*/
				asset_copy("bas/sound"); /*copy the example programs*/
			}
			asset_copy_res(getApplicationInfo().dataDir);
			X11basicView.setHomeDir(Basestoragedirectory.toString());
			/* Hier sind wir bereit, ein Programm zu starten, wenn gewünscht....*/

                        /*  Jetzt Intend Data auswerten.   */

			final android.content.Intent intent = getIntent ();
			if (intent != null) {
				android.util.Log.d (TAG, "> Got intent : " + intent);
				final String action = intent.getAction();
				/* If the intent is a request to create a shortcut, we'll do that and exit */
				if (android.content.Intent.ACTION_CREATE_SHORTCUT.equals(action)) {
					/* Hier sollten wir einen Dialog starten zum File auswaehlen.*/
					loadFileList(); 
					aBitmap = BitmapFactory.decodeResource(this.getResources(),R.drawable.bombe);
					showDialog(5);
					return;
				}
				
				/*Ansonsten jetzt den Filenamen des zu startenden Files herausfinden....*/
				final android.net.Uri data = intent.getData ();
				// final Bundle b = intent.getExtras();
				if (data != null) {
					android.util.Log.d (TAG, "> Got data   : " + data);

					final String filePath = data.getEncodedPath ();
					int index = filePath.lastIndexOf( '/' );
					String f=filePath;
					if(index>0) f=filePath.substring(index+1);
					if(!sessionretained || !mChosenFile.contentEquals(f)) {
						misloaded=true;
						mChosenFile=f;
						setTitle(mChosenFile);
						X11basicView.Loadandrun(filePath);
						Log.d(TAG, "Autostart Program: "+filePath);
					} else Log.d(TAG, "Continue Program: "+filePath);
				} 
			} 

			/* Wenn kein Programm über den Intend vorgegeben war, dann versucher autorun.inf auszuwerden...*/

			if(misloaded==false) {
				int index=autorunfile.lastIndexOf( '/' );
				mChosenFile=autorunfile;
				if(index>0) mChosenFile=autorunfile.substring(index+1);

				try {
					/* Wenn das Programm im Verzeichnis gefunden wurde .... */
					if(Arrays.asList(assetManager.list("bas")).contains(mChosenFile)) {
						misloaded=true;
						setTitle(mChosenFile);
						// geht nur, wenn das programm nicht im asset ist...
						// Hierfuer muessen wir im NDk noch eine Funktion machen
						// Welche ein Programm vom String aus laden kann.
						// Autorun.bas aus dem normalen /bas/ Ordner ist nicht
						// implementiert.
						// TODO
						X11basicView.Loadandrun(autorunfile);
						Log.d(TAG, "Autostart Program: "+autorunfile);
					} else {
						setTitle("X11-Basic");
						mChosenFile="new.bas";
					}
				} catch (IOException e) {
					e.printStackTrace();
					setTitle("X11-Basic");
				}
			}
			Log.d(TAG, "onCreate the first time done.");
		} else {
			// If there is a task already running, connect it to the new activity object.
			mBackgroundTask= (RunThread) retained;
			mBackgroundTask.resetActivity (this,screen);
			if(misloaded) setTitle(mChosenFile);
			if(mBackgroundTask.isAlive()) {
				//pd.show();
				Log.d(TAG, "BackgroundTask is alive. "+misloaded+" "+mChosenFile);
			}
			sessionretained=true;
		}
	}
	public static void setfontsize(final int fnt) {
		mfontsize=fnt;
		screen.setfontsize(mfontsize);
	}
	public static void setfocus(final int foc) {
		mfocus=foc;
		screen.setfocus(mfocus);
	}

	@Override
	protected void onPause(){
		super.onPause();
		Log.d(TAG,"OnPause");
	}
	@Override
	protected void onResume(){
		super.onResume();
		Log.d(TAG,"OnResume");
	} 
	@Override
	protected void onDestroy(){

		// Don't forget to shutdown tts!
		if (tManager != null) {
			tManager.shutdown();
			Log.d(TAG,"shutdown tts.");
		}

		super.onDestroy(); 
		if (isFinishing ()) endBackgroundTasks (true);
		Log.d(TAG,"OnDestroy");
	} 

	@Override
	protected void onStop(){
		super.onStop();
		Log.d(TAG,"OnStop");
		//X11basicView.Stop(); muss nicht sein.
		//  SharedPreferences settings = getPreferences(MODE_PRIVATE);
		//  SharedPreferences.Editor editor = settings.edit();
		//  editor.putInt("fontsize", mfontsize);

		//  editor.commit();
		/*This is a rude method to force a program end. Geht nicht, da sonst auch Ende bei anderer Activity*/
		// finish();
		//  int pid = android.os.Process.myPid();
		//  android.os.Process.killProcess(pid); 
	}  
	@Override
	protected void onRestart(){
		super.onRestart();
		Log.d(TAG,"OnRestart");
		//  screen.InitScreen(screen.mBitmap);
	}
	@Override
	public void onStart() {
		super.onStart();
		start_runengine();         /*    sorgt dafür dass die Programmausführung auch sicher weiterlaeuft... */
		_redrawHandler.sleep(50);  /*... und der screen refresh neu getriggert wird.*/
	} 

	public static RefreshHandler _redrawHandler = new RefreshHandler();
	static class RefreshHandler extends Handler {
		@Override
		public void handleMessage(Message message) {
			if(X11basicView.mredrawnec)  {
				screen.invalidate();
				X11basicView.mredrawnec=false;
			}
			else _redrawHandler.sleep(50);
		}
		public void sleep(long delayMillis) {
			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), delayMillis);
		}   
	};
	/**
	 * onRetainNonConfigurationInstance
	 * Called by the system, as part of destroying an activity due to a configuration change, 
	 * when it is known that a new instance will immediately be created for the new configuration.
	 * Return an object that can be retrieved with getLastNonConfigurationInstance.
	 *
	 * @return Object - object to be saved
	 */

	public Object onRetainNonConfigurationInstance () {
		if (mBackgroundTask != null) {
			mBackgroundTask.resetActivity (null,null);
		}
		return mBackgroundTask;
	}
	/**
	 * End any background tasks that might be running and clean up.
	 * 
	 * @param cleanup boolean - true means that any thread objects should be released
	 */

	private void endBackgroundTasks (boolean cleanup) {
		if(mBackgroundTask != null) {
			// If we are cleaning up, it means that the UI is no longer available or will soon be unavailable.
			if(cleanup) {mBackgroundTask.disconnect();}
			// Make sure the task is cancelled.
			mBackgroundTask.cancel (true);
			// Finish cleanup by removing the reference to the task
			if (cleanup) {
				mBackgroundTask = null;
				Log.d(TAG,"endBackgroundTasks: Interrupted and ended task.");
			} else Log.d(TAG,"endBackgroundTasks: Interrupted task.");
		}
	}

	public void ThreadComplete(boolean cancel) {
		if(cancel) Log.d(TAG,"BackgroundTask cancelled.");
		else Log.d(TAG,"BackgroundTask ended.");
		if(cancel) toaster(getResources().getString(R.string.word_interrupt));
		else toaster(getResources().getString(R.string.word_done));
		screen.invalidate();		
		mBackgroundTask.disconnect ();
		mBackgroundTask = null;
	}

	public void toaster(final String message) {
		Log.d(TAG,message);
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
	}

	private void setupShortcut(final String AppName, final String FileName, final Bitmap aBitmap){

		Intent shortcutIntent = new Intent(Intent.ACTION_MAIN);        // Tells launcher where what to launch
		shortcutIntent.setClassName(this, X11BasicActivity.class.getName());		 
		shortcutIntent.setData(Uri.parse(FileName));

		shortcutIntent.putExtra("FILE", FileName);		 // Use the full filename for the file to load
		shortcutIntent.putExtra("PWD", Basestoragedirectory.getAbsolutePath());		 // Use the full filename for the file to load

		Intent intent = new Intent();
		intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
		intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, AppName);          // Set the title

		intent.putExtra(Intent.EXTRA_SHORTCUT_ICON, aBitmap);			 // Set the icon bitmap
		setResult(RESULT_OK, intent);                                  // Tell caller all is ok
		Toast.makeText(getApplicationContext(), String.format(getResources().getString(R.string.message_shortcut_created), AppName), Toast.LENGTH_LONG).show();
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);        
		return true;
	} 
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		/*
		// Checks whether a hardware keyboard is available
		if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
			Toast.makeText(this, "hardware keyboard active", Toast.LENGTH_SHORT).show();
		} else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
			Toast.makeText(this, "no hardware keyboard", Toast.LENGTH_SHORT).show();
		}
		 */	
	}

	private void do_load(final String filename) {
		X11basicView.Load(Basestoragedirectory.getAbsolutePath()+"/bas/"+filename);
		misloaded=true;
		setTitle(filename);
		Log.d(TAG,"Program loaded: "+filename);
	}
	/* Startet X11-Basic im View */
	private void do_run() {
		X11basicView.Run();
		start_runengine();
	}
	public void done() {
		toaster(getResources().getString(R.string.word_done));
		screen.invalidate();		
	}
	/* Hier das Problem mit TTS und AsyncTask angehen....*/
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	static public <T> void executeAsyncTask(AsyncTask<T, ?, ?> task, T... params) {
	    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	      task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
	    }
	    else {
	      task.execute(params);
	    }
	  }
	private void start_runengine() {
		RunThread t = mBackgroundTask;
		if (t != null) {
			if(!t.isAlive()) toaster(getResources().getString(R.string.error_backgroundtask_dead));
			else {
				Log.d(TAG,"Background task is already running.");
				X11basicView.Putchar(10); /*to finish blocked input (maybe)*/
			}
		} else {
			/*Korrekturdaten lesen:*/
			// SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
			//	float pcorrlatency=prefs.getFloat("pcorr_latenz", (float)7*60);
			//	boolean lowpassp=prefs.getBoolean("lowpassp", false);
			//	int lowpassporder=prefs.getInt("lowpassporder",1);

			RunThread rct = new RunThread (this, screen);
			mBackgroundTask = rct;

			// Start the task. Pass two arguments (integer).  but this is just a
			// mock-up so the values do not really matter.
			// rct.execute(4711, 815); Alternativ:
			executeAsyncTask(rct,4711, 815);
			Log.d(TAG, "Run Thread started."); 
		}
	}
	private void do_compile() {
		/*Teste, ob ein Programm geladen wurde, wenn nicht --> Toast
		 * generiere outputfilenamen aus input
		 * rufe native method compile auf. */
		if(misloaded) {
			String ofilename=mChosenFile.replace(".bas", ".b");
			String pfilename=Basestoragedirectory.getAbsolutePath()+"/bas/"+ofilename;

			int ret=X11basicView.Compile(pfilename);
			if(ret==0) toaster(String.format(getResources().getString(R.string.message_compilation_success),pfilename));
			else if(ret==-1) toaster(getResources().getString(R.string.message_nothing_to_do));
			else toaster(getResources().getString(R.string.error_compilation));
		} else toaster(getResources().getString(R.string.message_nothing_to_do2));
	} 

	private int dialogi=0;  


	private Dialog webDialog(final String title, final String text) {
		final Dialog dialog = new Dialog(this);
		if(title==null || title=="") dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		else dialog.setTitle(title);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(R.layout.webdialog);
		TextView tV= (TextView) dialog.findViewById(R.id.TextView01);
		tV.setText(getResources().getString(R.string.word_searchcomment));
		final WebView wV=(WebView) dialog.findViewById(R.id.webView1);
		if(text.length()>0) wV.loadDataWithBaseURL(null,"<html><body>"+text+"</body></html>" , "text/html", "utf-8", null);
		else wV.loadDataWithBaseURL(null,"<html><body>"+Help.getonlinehelp(getAssets(),"_")+"</body></html>" , "text/html", "utf-8", null);
		//set up button
		Button button = (Button) dialog.findViewById(R.id.Button01);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		EditText eT=(EditText) dialog.findViewById(R.id.editText1);
		//Set the last entered command
		eT.addTextChangedListener(new TextWatcher(){
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			public void afterTextChanged(Editable s) {
				if(s.length()>0) {
					wV.loadDataWithBaseURL(null,"<html><body>"+Help.getonlinehelp(getAssets(),s.toString())+"</body></html>" , "text/html", "utf-8", null);
				} else {
					//	wV.loadDataWithBaseURL(null,"<html><body>"+Help.getonlinehelp(getAssets(),"_")+"</body></html>" , "text/html", "utf-8", null);
				}
			}
		});
		return dialog;
	}
	private Dialog infoDialog(final String title, final String text) {
		final Dialog dialog = new Dialog(this);
		if(title==null || title=="") dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		else dialog.setTitle(title);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(R.layout.webdialog);
		TextView tV= (TextView) dialog.findViewById(R.id.TextView01);
		tV.setVisibility(View.GONE);
		TextView tV2= (TextView) dialog.findViewById(R.id.textView1);
		tV2.setVisibility(View.GONE);
		final WebView wV=(WebView) dialog.findViewById(R.id.webView1);
		if(text.length()>0) wV.loadDataWithBaseURL(null,"<html><body>"+text+"</body></html>" , "text/html", "utf-8", null);
		else wV.loadDataWithBaseURL(null,"<html><body>"+Help.getreleasenotes(getAssets())+"</body></html>" , "text/html", "utf-8", null);
		//set up button
		Button button = (Button) dialog.findViewById(R.id.Button01);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		EditText eT=(EditText) dialog.findViewById(R.id.editText1);
		eT.setVisibility(View.GONE);
		return dialog;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent viewIntent;
		// 	speek("I am going to open the menu item. Be patient...",(float)0.8,(float)0.8);
		switch (item.getItemId()) {
		case R.id.infotext:    
			viewIntent = new Intent(X11BasicActivity.this, AboutActivity.class);
			startActivity(viewIntent);
			break; 
		case R.id.load:
			loadFileList(); 
			showDialog(10+dialogi++);
			break;
		case R.id.run: 
			do_run();
			break;
		case R.id.compile: 
			do_compile();
			break;
		case R.id.editor: 
			call_editor();
			break;
		case R.id.newnew: 
			X11basicView.New();
			mChosenFile="new.bas";
			setTitle("X11-Basic");
			toaster("New.");
			break;
		case R.id.stop: 
			X11basicView.StopCont();
			break;
		case R.id.finish: 
			finish();
			/*Dafeur sorgen, dass auch wirklich alles beendet wird.*/
			int pid = android.os.Process.myPid();
			android.os.Process.killProcess(pid); 
			break;
		case R.id.help:
			Dialog dialog = webDialog(getResources().getString(R.string.menu_help),"");
			dialog.show();
			break;              
		case R.id.keyboard: 
			InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			// only will trigger it if no physical keyboard is open
			// mgr.showSoftInput(screen, InputMethodManager.SHOW_IMPLICIT);

			mgr.toggleSoftInputFromWindow (screen.getWindowToken (),
					InputMethodManager.SHOW_FORCED,
					InputMethodManager.HIDE_IMPLICIT_ONLY );
			break;
		case R.id.paste: 
			doPaste();
			break;
		case R.id.settings:     
			viewIntent = new Intent(X11BasicActivity.this, PreferencesActivity.class);
			startActivity(viewIntent);
			break;
		default: 
			return super.onOptionsItemSelected(item);
		}
		return true;
	}



	/*Lade die X11-basic Library beim kreieren dieser Activity.*/

	static {
		try {System.loadLibrary("gmp");
		     System.loadLibrary("x11basic");}
		catch (UnsatisfiedLinkError ule) {
			Log.e(TAG, "WARNING: Could not load libx11basic.so or libgmp.so");
		}
	}

	private Dialog scrollableDialog(final String title, final String text) {
		final Dialog dialog = new Dialog(this);
		if(title!=null && title!="") dialog.setTitle(title);
		else dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(R.layout.maindialog);
		TextView wV= (TextView) dialog.findViewById(R.id.TextView01);
		wV.setText(Html.fromHtml(text));
		//set up button
		Button button = (Button) dialog.findViewById(R.id.Button01);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		return dialog;
	}
	public class Item {
		public String text;
		public int icon;
		public Item(String text, Integer icon) {
			this.text = text;
			this.icon = icon;
		}
		@Override
		public String toString() {
			return text;
		}
	}

	@Override
	protected Dialog onCreateDialog(final int id) {
		Dialog dialog = null;
		if(id==0) { /* Verision info*/
			dialog=infoDialog("New in this version:","");
		} else if(id==3) {   /*The delete file dialog*/
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(getResources().getString(R.string.message_really_delete));
			builder.setMessage(String.format(getResources().getString(R.string.message_delete), mSelectedFile));
			builder.setPositiveButton(getResources().getString(R.string.word_proceed), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					File f=new File(Basestoragedirectory+"/bas/"+mSelectedFile);
					f.delete();
				} }); 
			builder.setNeutralButton(getResources().getString(R.string.word_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					;
				} }); 
			dialog = builder.create();
			dialog.setCanceledOnTouchOutside(false);

		} else if(id==4) { /* Dialog Filemenu*/
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(mSelectedFile);
			builder.setIcon(R.drawable.bombe);
			final String[] mitem={
					"LOAD","MERGE","LOAD+RUN","LOAD+LIST","LOAD+edit","LOAD+compile","compile+RUN","delete","Cancel"
			};
			builder.setItems(mitem, new DialogInterface.OnClickListener(){
				@Override 
				public void onClick(DialogInterface dialog, int which){
					Log.d(TAG,"Item #"+which+"was clicked");
					if(which==0 || which==2 || which==3 || which==4|| which==5|| which==6) {
						mChosenFile = mSelectedFile;
						do_load(mChosenFile);
					} else if(mitem[which].equalsIgnoreCase("delete")) { /*delete*/
						showDialog(3);
						mad.dismiss();
					}
					if(which==4) call_editor();
					if(which==5||which==6) do_compile();
					if(which==6) {
						mChosenFile = mChosenFile.replace(".bas", ".b");
						do_load(mChosenFile);
					}
					if(which==2 || which==6) {mad.dismiss(); do_run();}

				}});
			dialog = builder.show();
		} else if(id==5) { /* Dialog zum File auswählen fuer Shortcut*/
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(getResources().getString(R.string.message_select_file_shortcut));
			builder.setIcon(R.drawable.bombe);
			if(mFileList == null){
				dialog = builder.create();
				return dialog;  
			}  
			builder.setItems(mFileList, new DialogInterface.OnClickListener(){
				@Override 
				public void onClick(DialogInterface dialog, int which){
					mChosenFile = mFileList[which];
					String FullFileName=Basestoragedirectory.getAbsolutePath()+"/bas/"+mChosenFile;
					String AppName = mChosenFile;
					setupShortcut( AppName, FullFileName, aBitmap);         // Go finish the shortcut setup
					finish(); /* beenden */
				}});
			dialog = builder.show();
		} else if(id==7) { /*Dialog, SD Card nicht verfügbar.*/
			dialog=scrollableDialog("ERROR",MessageFormat.format(getResources().getString(R.string.sdcard_notb),Basestoragedirectory));
		} else if(id==8) {/* Dialog Fehlermeldung kann nicht editieren */
			/* "Compiled code cannot be edited." */
			dialog=scrollableDialog("ERROR",getResources().getString(R.string.editor_notb));
		}  else if(id==9) {/* Dialog Fehlermeldung kein Editor */
			dialog=scrollableDialog("ERROR",getResources().getString(R.string.editor_notfound));
		}  else  {  /* Dialog zum File auswählen fuer Programm laden...*/
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(getResources().getString(R.string.message_select_file_load));
			builder.setIcon(R.drawable.bombe);
			if(mFileList == null){
				dialog = builder.create();
				return dialog;  
			}
			items = new Item[mFileList.length];
			for(int i=0;i<mFileList.length;i++) {

				items[i]=new Item(mFileList[i],0);
				if(mFileList[i].endsWith(".b")) items[i]=new Item(mFileList[i],android.R.drawable.ic_menu_slideshow);
				else if(mFileList[i].endsWith(".bas")) items[i]=new Item(mFileList[i],android.R.drawable.ic_menu_view);
				else items[i]=new Item(mFileList[i],0); /*no icon.*/
			}
			ListAdapter adapter = new ArrayAdapter<Item>(this,android.R.layout.select_dialog_item,android.R.id.text1,items){
				public View getView(int position, View convertView, ViewGroup parent) {
					//User super class to create the View
					View v = super.getView(position, convertView, parent);
					TextView tv = (TextView)v.findViewById(android.R.id.text1);

					//Put the image on the TextView
					tv.setCompoundDrawablesWithIntrinsicBounds(X11BasicActivity.items[position].icon, 0, 0, 0);

					//Add margin between image and text (support various screen densities)
					int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
					tv.setCompoundDrawablePadding(dp5);
					return v;
				}
			};

			//	    builder.setItems(mFileList, 


			builder.setAdapter(adapter,  new DialogInterface.OnClickListener(){
				@Override 
				public void onClick(DialogInterface dialog, int which){
					mChosenFile = mFileList[which];
					do_load(mChosenFile);
				}});
			mad = builder.create(); //don't show dialog yet
			mad.setOnShowListener(new OnShowListener() {       
				@Override
				public void onShow(DialogInterface dialog) {
					ListView lv = mad.getListView(); //this is a ListView with your "buds" in it
					lv.setOnItemLongClickListener(new OnItemLongClickListener() {
						@Override
						public boolean onItemLongClick(AdapterView<?> parent, View view, int which, long id) {
							Log.d("Long Click!","List Item #"+which+" was long clicked");
							mSelectedFile = mFileList[which];
							showDialog(4);
							return true;
						}           
					});     
				}
			});
			dialog = mad;
		}
		return dialog;
	}
	/* Wird aufgerufen, jedesmal befor der Dialog angezeigt wird.*/
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		super.onPrepareDialog(id, dialog);

		switch(id) {
		case 3:
			((AlertDialog) dialog).setMessage(String.format(getResources().getString(R.string.message_delete), mSelectedFile));
			break;
		case 4:
			((AlertDialog) dialog).setTitle(mSelectedFile);
			break;
		default:
			break;
		}
	}
	private void loadFileList() {
		File dirdata=new File(Basestoragedirectory+"/bas");
		dirdata.mkdir();
		if(dirdata.exists()){
			FilenameFilter filter = new FilenameFilter(){
				public boolean accept(File dir, String filename){
					File sel = new File(dir, filename);
					return (filename.endsWith(".bas") || filename.endsWith(".b")) && !sel.isDirectory();
				}
			};
			mFileList = dirdata.list(filter);


			Arrays.sort(mFileList, new Comparator<String>() {
				@Override
				public int compare(String entry1, String entry2) {
					return entry1.compareToIgnoreCase(entry2);
				} 
			});
			mcheckitems=new boolean[mFileList.length];
			for(int i=0;i<mFileList.length;i++) mcheckitems[i]=true;
		} else {
			Log.d(TAG,"Path not found! "+dirdata);
			mFileList= new String[0]; 
			mcheckitems=new boolean[0];
		}
	}

	private static void copyFile(final InputStream in, final OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while((read = in.read(buffer)) != -1){
			out.write(buffer, 0, read);
		} 
	}


	/*Copy the example programs from assets to bas folder*/
	private void asset_copy(final String path) {
		File dirdata=new File(Basestoragedirectory+"/"+path);
		dirdata.mkdir();
		File destination; 

		AssetManager assetManager = getAssets();
		String[] files = null; 
		try {
			files = assetManager.list(path);
		} catch (IOException e) { Log.e(TAG, e.getMessage());}

		for(String filename : files) {
			InputStream in = null;
			OutputStream out = null;
			try {
				destination=new File(dirdata+"/" + filename);
				if(destination.exists()) {
					Log.d(TAG,"File "+destination+" already there. will not copy.");
				} else {
					in = assetManager.open(path+"/"+filename);
					out = new FileOutputStream(dirdata+"/" + filename);

					Log.d(TAG,"copy file: "+filename+" -->"+dirdata+"/");
					copyFile(in, out);
					in.close();
					in = null;
					out.flush();
					out.close();
					out = null;
				}
			} catch(Exception e) {
				Log.e(TAG, e.toString()+e.getMessage());
			}       
		}
	}
	/*Copy the res folder content from assets to internal data folder*/
	private void asset_copy_res(final String  datapath) {
		File dirdata=new File(datapath);
		dirdata.mkdir();
		File destination; 

		AssetManager assetManager = getAssets();
		String[] files = null; 
		try {
			files = assetManager.list("res");
		} catch (IOException e) { Log.e(TAG, e.getMessage());}

		for(String filename : files) {
			InputStream in = null;
			OutputStream out = null;
			try {
				destination=new File(dirdata+"/" + filename);
				
				if(destination.exists()) {
					Log.d(TAG,"File "+destination+" already there. will not copy.");
				} else {
					in = assetManager.open("res/"+filename);
					out = new FileOutputStream(dirdata+"/" + filename);
					Log.d(TAG,"copy file: "+filename+" -->"+dirdata+"/");
					copyFile(in, out);
					in.close();
					in = null;
					out.flush();
					out.close();
					out = null;
				}
				
			} catch(Exception e) {
				Log.e(TAG, e.toString()+e.getMessage());
			}       
		}
	}



	/*  Funktionen fuer Context Menu (copy paste text)
	 * 
	 * */



	private final static int SELECT_TEXT_ID = 0;
	private final static int COPY_ALL_ID = 1;
	private final static int PASTE_ID = 2;
	// private final static int SEND_CONTROL_KEY_ID = 3;
	// private final static int SEND_FN_KEY_ID = 4;


	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle(R.string.edit_text);
		menu.add(0, SELECT_TEXT_ID, 0, R.string.select_text);
		menu.add(0, COPY_ALL_ID, 0, R.string.copy_all);
		menu.add(0, PASTE_ID, 0, R.string.paste);
		//      menu.add(0, SEND_CONTROL_KEY_ID, 0, R.string.send_control_key);
		//      menu.add(0, SEND_FN_KEY_ID, 0, R.string.send_fn_key);
		if (!canPaste()) {
			menu.getItem(PASTE_ID).setEnabled(false);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case SELECT_TEXT_ID:
			//        screen.toggleSelectingText();
			return true;
		case COPY_ALL_ID:
			doCopyAll();
			return true;
		case PASTE_ID:
			doPaste();
			return true;
			//          case SEND_CONTROL_KEY_ID:
			//            doSendControlKey();
			//            return true;
			//          case SEND_FN_KEY_ID:
			//            doSendFnKey();
			//            return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	private boolean canPaste() {
		ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		if (clip.hasText()) {
			return true;
		}
		return false;
	}


	private void doCopyAll() {
		// ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		//    clip.setText(screen.getTranscriptText().trim());
	}

	private void doPaste() { 
		ClipboardManager clip = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		CharSequence paste = clip.getText();
		String s=null;
		if(paste!=null) s = paste.toString();
		if(s!=null) screen.write(s);
	}

	/**
	 * Determine if the app is running with a device or an emulator.
	 */
	public static boolean isDevice() {
		return !"sdk".equals(Build.MODEL) 
				&& !"sdk".equals(Build.PRODUCT) 
				&& !"generic".equals(Build.DEVICE);
	}

	public void call_editor() {
		Log.d(TAG,"call editor with "+mChosenFile);
		if(mChosenFile.endsWith(".b")) showDialog(8);
		else {
			Intent intent = new Intent(Intent.ACTION_EDIT);	
			File dirdata=new File(Basestoragedirectory+"/"+"bas/");
			dirdata.mkdir();
			File f=new File(dirdata,mChosenFile);
			
			if(!f.exists()) {
				try {
					f.createNewFile();
					FileOutputStream fos=null;
					OutputStreamWriter osw=null;
					Calendar cal = Calendar.getInstance();
					fos=new FileOutputStream(f,true);
					osw=new OutputStreamWriter(fos);
					osw.write("' new X11-Basic File "+f.getAbsolutePath()+" created on "+cal.getTime().toLocaleString()+"\n");
					osw.write("PRINT \"This is the dummy new.bas, which\"\n");
					osw.write("PRINT \"has not been written yet.\"\nEND\n");
					osw.flush();
					osw.close();
					fos.close();
					
					Log.d(TAG,"new file created: "+f);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			Uri uri = Uri.fromFile(f);
			Log.d(TAG,uri.toString());
			intent.setDataAndType(uri, "text/plain"); 
			try {startActivityForResult(intent,1);}
			catch(ActivityNotFoundException e) {
				//showDialog(9);
				toaster(String.format(getResources().getString(R.string.error_editor), e.getMessage()));
				showDialog(9);
			}
			catch(SecurityException e) {
				//showDialog(9);
				toaster(String.format(getResources().getString(R.string.error_editor), e.getMessage()));
				showDialog(9);
			}
		}
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {  /* 1 kommt vom Editor-Aufruf*/

			if(resultCode == RESULT_OK){
				//	      String result=data.getStringExtra("result");
				Log.d(TAG,"Result OK from editor.");
			}

			if (resultCode == RESULT_CANCELED) {
				Log.d(TAG,"Result CANCEL from editor.");
			}
			// Hier jetzt Datei neu laden.
			do_load(mChosenFile);
			toaster(String.format(getResources().getString(R.string.message_reloaded), mChosenFile));
		} else if (requestCode == 2) {  /* 1 kommt vom call_intent */
			/* Das jetzt an x11basic zurueckmelden....*/
			/*  TODO : */
		}
	}


	public void call_intent(final String action, final String data, final String extra) {
		//TODO :
		Intent intent = new Intent(action); 
		Uri uri = Uri.parse(data); 
		//	intent.setDataAndType(uri, "text/plain");
		intent.setData(uri);
		// intent.putExtra(name, value);
		try {startActivityForResult(intent,2);}
		catch(ActivityNotFoundException e) {
			toaster("ERROR: call intent: "+e.getMessage()+String.format(getResources().getString(R.string.error_intend), action+": "+e.getMessage()));
		}
	}

	public  void speek(final String text,final float pitch,final float rate,final int locale ) {
		if(tManager==null) toaster("ERROR: speach engine not ready.");
		else {
		   if(locale>=0) tManager.setLanguage(locale);
		   if(pitch>0)  tManager.setPitch(pitch);
		   if(rate>0)   tManager.setRate(rate);
		   tManager.speak(text);
		}
	}
	private static MediaPlayer mp;
	public void beep() { 
		mp = MediaPlayer.create(X11BasicActivity.this, net.sourceforge.x11basic.R.raw.bell);   
		mp.start();
		mp.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.release();
			}
		});
	}
	public void hidek() { 
          // Check if no view has focus:
          View view = this.getCurrentFocus();
          if (view != null) {  
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
          }
	}
	public void showk() { 
          // Check if no view has focus:
          View view = this.getCurrentFocus();
          if (view != null) {  
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
          }

	}
	private static MediaPlayer mp2;
	public void playsoundfile(final String fname) { 
		try {
			if (mp2 == null) mp2 = new MediaPlayer();
			else mp2.reset();   // so can change data source etc.
			mp2.setDataSource(fname);
		} catch (IllegalArgumentException e1) {
			e1.printStackTrace();
		} catch (IllegalStateException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			Log.e(TAG, "IO Error!");
			e1.printStackTrace();
		}
		// mp2.prepareAsync(); // prepare async to not block main thread
		try {
			mp2.prepare();
			mp2.start();
			mp2.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					mp2.release();
					mp2=null;
				}
			});
		} catch(Exception e){
			// handle error here..
		}
	}
	void messageBox(final String title, final String msg, final int flag) {
		runOnUiThread(new Runnable(){public void run(){
			if( title == null )
				Toast.makeText(X11BasicActivity.this, msg, Toast.LENGTH_SHORT).show();
			else new AlertDialog.Builder(X11BasicActivity.this)
			.setTitle(title).setMessage(msg).setIcon( (flag == 0)
					? android.R.drawable.ic_dialog_info
							: android.R.drawable.ic_dialog_alert).show();
		}});
	}
	static boolean tryEmailAuthor(final Context c, final boolean isCrash, final String body) {
		final String addr = c.getString(R.string.author_email);
		final Intent i = new Intent(Intent.ACTION_SEND);
		String modVer = "";
		try {
			Process p = Runtime.getRuntime().exec(new String[]{"getprop","ro.modversion"});
			modVer = readAllOf(p.getInputStream()).trim();
		} catch (Exception e) {}
		if (modVer.length() == 0) modVer = "original";
		// second empty address because of http://code.google.com/p/k9mail/issues/detail?id=589
		i.putExtra(Intent.EXTRA_EMAIL, new String[]{addr, ""});
		i.putExtra(Intent.EXTRA_SUBJECT, MessageFormat.format(c.getString(
				isCrash ? R.string.crash_subject : R.string.email_subject),
				getVersion(c), Build.MODEL, modVer, Build.FINGERPRINT));
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_TEXT, body!=null ? body : "");
		try {
			c.startActivity(i);
			return true;
		} catch (ActivityNotFoundException e) {
			try {
				// Get the OS to present a nicely formatted, translated error
				c.startActivity(Intent.createChooser(i,null));
			} catch (Exception e2) {
				e2.printStackTrace();
				Toast.makeText(c, e2.toString(), Toast.LENGTH_LONG).show();
			}
			return false;
		}
	} 
	static String readAllOf(final InputStream s) throws IOException {
		final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(s),8096);
		String line;
		StringBuilder log = new StringBuilder();
		while ((line = bufferedReader.readLine()) != null) {
			log.append(line);
			log.append("\n"); 
		} 
		return log.toString();
	}  
	static String getVersion(final Context c) { 
		try {
			return c.getPackageManager().getPackageInfo(c.getPackageName(),0).versionName;
		} catch(Exception e) { return c.getString(R.string.unknown_version); }
	}
	void nativeCrashed(final int sig, final String message) {
		Log.e(TAG,"native CRASH!");
		// final RuntimeException r=	new RuntimeException("crashed here: ");
		
		String e="";
		if(sig==11) e="** 1 - Segmentation fault : Speicherschutzverletzung";
		else if(sig==10) e=  "** 2 - Bus Error Peek/Poke falsch?";
		else if(sig==11) e=  "** 3 - Adress error Ungerade Wort-Adresse! Dpoke/Dpeek, Lpoke/Lpeek?";
		else if(sig==4) e=  "** 4 - Illegal Instruction : ungültiger Maschinenbefehl";
		else if(sig==8) e=  "** 5 - Divide by Zero : Division durch Null";
		else if(sig==11) e=  "** 6 - CHK exeption : CHK-Befehl";
		else if(sig==5) e=  "** 7 - TRAPV exeption : TRAPV-Befehl";
		else if(sig==11) e=  "** 8 - Privilege Violation : Privilegverletzung";
		else if(sig==13) e=  "** 10 - Broken pipe : Ausgabeweitergabe abgebrochen";
		else if(sig==23) e=  "** I/O Exception : Eingabe/Ausgabefehler";
		else if(sig==3) e=  "** SIGQUIT ???";
		else if(sig==6) e=  "** SIGABRT suicide???";
		String a="";
		//errorlog="test";

		for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
		    a+=ste+"\n";
		}

		final Intent newIntent=new Intent(this, CrashHandler.class);
		
		newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		newIntent.putExtra(Intent.EXTRA_TEXT, "Signal="+sig+"("+e+")\n"+message+
				"\nstack="+a
//				+"\nstack="+r.toString()+
//				"\nmore="+Log.getStackTraceString(r.getCause())
				);
		startActivity(newIntent);
	}
} 
