package net.sourceforge.x11basic;

/* RunThread.java (c) 2011-2015 by Markus Hoffmann
 *
 * This file is part of X11-Basic for Android, (c) by Markus Hoffmann 1991-2015
 * ============================================================================
 * X11-Basic for Android is free software and comes with 
 * NO WARRANTY - read the file COPYING for details.
 */ 
 
import android.os.AsyncTask;
import android.util.Log;

/* Über diese Klasse werden einige Routinen aus der X11BasicActivity im 
   Hintergrund ausgeführt. Dies Erstellt den entsprechenden Thread.
   Die Routine doInBackground() starten den Thread und läßt 
   X11basicView.Programmlauf(); ausführen.
   */

public class RunThread extends AsyncTask<Integer, Integer, Integer> {
	private static final String TAG = "X11-Basic RUNTHREAD";
	private boolean mCompleted=false;
	private boolean mCancelled=false;
	private boolean isalive=false;
	private X11BasicActivity mActivity=null;
	X11basicView mScreen=null;
	/*Initialisiere und merke Context (X11BasicActivity) 
	  und screen (X11basicView) */

	public RunThread(final X11BasicActivity context,final X11basicView screen) {
		mActivity = context;	
		mScreen=screen;
	}  

	public void disconnect () {
		if (mActivity != null) {
			mActivity = null;
			Log.d(TAG, "RunThread has successfully disconnected from the activity.");
		}
	} 
	public boolean isAlive() {
		return isalive;
	}
	
	@Override
	protected Integer doInBackground(Integer... params) {
		isalive=true;
		X11basicView.Programmlauf(); 
		Log.d(TAG, "inBackground completed.");
		isalive=false;
		return null;
	}
	/*
	 * Hm.... nur für den Fall dass....
	 * Das mit dem Progressdialog, kann man sich auch knicken...
	 * läuft im UI thread .
	 */

	@Override 
	protected void onProgressUpdate(Integer... info) {
	  //  if (mActivity != null) mActivity.updateProgress(info [0]);
	}
	
	/*
	 * Wenn irgendwas zu einem Abbruch führt....
	 * läuft im UI thread.
	 */

	@Override 
	protected void onCancelled () {
	    mCompleted = true;
	    mCancelled = true;
	    isalive=false;
	    X11basicView.Stop();
	    X11basicView.Putchar(10); /*to finish blocked input*/ 

	    if (mActivity != null) mActivity.ThreadComplete (mCancelled);
	    disconnect();
	}
	
	@Override
    	protected void onPreExecute() {
		super.onPreExecute();
		// if (mActivity != null) mActivity.prepare_progressdialog();
    	}
	/*
	 * Dies wird aufgerüfen, nachdem der Task beendet wurde oder
	 * zuende gelaufen ist.
	 * Läuft im UI thread.
	 */

	@Override 
	protected void onPostExecute (Integer result)  {
	    mCompleted = true;
	    if (mActivity != null) mActivity.ThreadComplete(mCancelled);
	    disconnect();
	}
	/**
	 * Thread neu starten (z.B. wenn der Screen rotiert wurde....
	 *
	 * Im einfachsten Fall werden nur die X11BasicActivity activity und
	 * X11basicView screen erneuert. Der Thread läuft einfach weiter.
	 * 
	 */

	public void resetActivity(final X11BasicActivity activity,final X11basicView screen) {
	    mActivity=activity;
	    mScreen=screen;

	    /* Wenn der Task inzwischen (von selbst) beendet hatte 
	      nochmal zur Sicherheit....
	     */
	    if (mCompleted && (mActivity != null)) {
	       mActivity.ThreadComplete(mCancelled);
	       disconnect();
	    }
	} 	
}
