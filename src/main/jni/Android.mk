LOCAL_PATH := $(call my-dir)
export MAINDIR:= $(LOCAL_PATH)

include $(CLEAR_VARS)

LOCAL_MODULE    := x11basic
LOCAL_SRC_FILES := 5x7.c 8x16.c android.c array.c \
                   terminal.c wort_sep.c virtual-machine.c xbasic.c \
                   variablen.c parser.c number.c parameter.c functions.c \
		   decode.c errortxt.c sfunctions.c afunctions.c io.c aes.c \
		   svariablen.c do_gets.c window.c file.c ltext.c framebuffer.c \
			bitmap.c graphics.c mathematics.c runtime.c sysVstuff.c \
			gkommandos.c kommandos.c main.c memory.c sound.c \
			consolefont.c bytecode.c xbbc.c fft.c unifont.c \
			unifont57.c raw_mouse.c raw_keyboard.c tools.c io_basic.c \
			loadprg.c type.c lodepng.c spat-a-fnt.c md5.c sha1.c


LOCAL_CFLAGS := -fsigned-char

LOCAL_LDLIBS    := -lm -llog -ljnigraphics 
LOCAL_SHARED_LIBRARIES := gmp
LOCAL_STATIC_LIBRARIES  = lapack

include $(BUILD_SHARED_LIBRARY)


include $(CLEAR_VARS)

LOCAL_MODULE            := gmp
LOCAL_SRC_FILES         := gmp/$(TARGET_ARCH_ABI)/libgmp.so


LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/gmp/$(TARGET_ARCH_ABI)

include $(PREBUILT_SHARED_LIBRARY)


#LAPACK, BLAS, F2C compilation
include $(CLEAR_VARS)
include $(MAINDIR)/clapack/Android.mk
LOCAL_PATH := $(MAINDIR)
include $(CLEAR_VARS)
LOCAL_MODULE:= lapack
LOCAL_STATIC_LIBRARIES := tmglib clapack1 clapack2 clapack3 blas f2c
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_C_INCLUDES)
LOCAL_EXPORT_LDLIBS := $(LOCAL_LDLIBS)
include $(BUILD_STATIC_LIBRARY)
#sqlite binary for assets
include $(CLEAR_VARS)
include $(MAINDIR)/sqlite/Android.mk
#gfalist binary for assets
include $(CLEAR_VARS)
include $(MAINDIR)/gfalist/Android.mk
