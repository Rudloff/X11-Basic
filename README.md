X11-Basic for Android
=====================

(c) 1991-2018 by Markus Hoffmann <kollo@users.sourceforge.net>

X11-Basic is a dialect of the BASIC programming language with graphics
capability. It has a very rich command set, though it is still easy to learn.
The syntax is most similar to GFA-Basic for ATARI-ST. It is a structured dialect
with no line numbers. A full manual and command reference is available.

GFA-programs should run with only a few changes. Also DOS/QBASIC programmers
will feel comfortable.

This implementation is one of the fastest basic interpreters for Android.
Programs can be compiled into a platform independant bytecode.

You can directly type in commands and formulas, which are beeing evaluated. This
way the interpreter works also as a pocket calculator. It supports complex
numbers and big integers.

Launcher shortcuts can be placed on the desktop to directly excecute BASIC
programs.

Basic programs can be written with any (third party) text editor.

The BASIC programs must be placed into the bas folder (/mnt/sdcard/bas). You can
find many more example programs on the X11-Basic homepage:
http://x11-basic.sourceforge.net/

X11-Basic will only work on devices with external storage (SD-Card or such). 

The WRITE EXTERNAL STORAGE permission is needed to write to the file system, The
COARSE and FINE LOCATION permissions are needed to get the location with the GPS
commands. The INTERNET permission is needed for all of the internet i/o
functions to work.


X11-Basic fuer Android
======================

(c) 1991-2018 by Markus Hoffmann <kollo@users.sourceforge.net>

Ein BASIC Interpreter und Compiler


X11-BASIC ist ein Dialekt der Programmiersprache BASIC.

Der Dialekt ist stark an GFA-Basic (f�r den ATARI ST) angelehnt. Es verf�gt �ber
einen reichen Befehlssatz, viele Grafik-Kommandos und auch spezielle Kommandos
zum Ausnutzen der Besonderheiten der Smartphones und Tablet-Computer. Die
Sprache ist dennoch leicht zu erlernen. Ein komplettes Handbuch steht zur
Verf�gung (leider bisher nur auf Englisch).

GFA-Programme sollen mit nur wenigen �nderungen ausgef�hrt werden k�nnen. Auch
DOS/QBASIC Programmierer werden sich wohl f�hlen.

Diese Implementierung ist eine der schnellsten Interpreter der
Programmiersprache BASIC f�r Android.

Der Interpreter ist mit einer kompletten VT100/ANSI Terminalemulation
ausgestattet. Au�erdem mit einem 16-Kanal ADSR-Sound-Synthesizer.

Sie k�nnen im Direktmodus Befehle und Formeln eingeben, die sofort ausgewertet
werden. So haben Sie auch gleich einen Taschenrechner.

Basic-Programme k�nnen mit jedem Text-Editor (z.B. Ted) geschrieben werden.

Die BASIC-Programme m�ssen in den bas-Ordner (/mnt/sdcard/bas) platziert werden.
Einige Beispielprogramme sind mit dabei. Auf der Homepage
http://x11-basic.sourceforge.net/ gibt es viele weitere Beispielprogramme.

Die WRITE EXTERNAL STORAGE Erlaubnis ist erforderlich, um auf das Dateisystem
schreiben zu k�nnen, Die LOCATION Erlaubnisse sind n�tig, um den Standort mit
den GPS-Befehlen lesen zu k�nnen. Die INTERNET Erlaubniss ist n�tig, damit die
internet-Kommandos funktionieren.


    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.



Acknowledgements
================

Thanks to all people, who helped me to realize this package.

Many  thanks  to  the developers of GFA-Basic. This basic made me
start programming in the 1980s. Many ideas and most of  the  command 
syntax has been taken from the ATARI ST implementation.

Thanks to sourceforge.net and gitlab.com for hosting this project on the web.

I would like to thank every people who help me out with source code, 
patches, program examples, bug tracking, help and documentation writing, 
financial support, judicious remarks, and so on...   

And here thanks to people, who helped me recently:

in 2012:
* Marcos Cruz (beta testing and bug fixing)
* Bernhard Rosenkraenzer (send me a patch for 64bit version)

in 2013:
* Matthias Vogl (\verb|va_copy| patch for 64bit version)
* Eckhard Kruse (for permission to include ballerburg.bas in the samples)
* Stewart C. Russell (helped me fix some compilation bugs on Raspberry PI)
* Marcos Cruz (beta testing and bug fixing)
* James V. Fields (beta testing and correcting the manual)

in 2014:
* John Clemens, Slawomir Donocik, Izidor Cicnapuz, Christian Amler,
  Marcos Cruz, Charles Rayer, Bill Hoyt, and John Sheales (beta testing and 
  bug fixing),
  Nic Warne and Duncan Roe for helpful patches for the linux target.

in 2015:
* Guillaume Tello, Wanderer, and John Sheales  (beta testing and bug fixing)

in 2016:
* John Sheales  (beta testing and bug fixing)
* bruno xxx (helping with the compilation tutorial for Android)

in 2017:
* John Sheales  (beta testing and bug fixing)
* David Hall  (bug fixing)
* Emil Schweikerdt  (bug fixing)


X11-Basic is build on top of many free softwares, and could not exist without 
them.

X11-Basic uses the Fast discrete Fourier and cosine transforms and inverses 
by Monty <xiphmont@mit.edu> released to the public domain from 
THE OggSQUISH SOFTWARE CODEC.

X11-Basic uses functionallity of the gmp library, 
the GNU multiple precision arithmetic library, Version 5.1.3.
   Copyright 1991, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000,
2001, 2002, 2003, 2004, 2005, 2006, 2007 Free Software Foundation, Inc.

For details, see: https://gmplib.org/

X11-Basic also uses functionallity if the LAPACK library. 
LAPACK (Linear Algebra Package) is a standard software library for 
numerical linear algebra. 

For details, see: http://www.netlib.org/lapack/

X11-Basic uses the lodepng code for the PNG bitmap graphics support.
Copyright (c) 2005-2013 Lode Vandevenne

X11-Basic uses a md5 algorithm  Copyright (c) 2001 by Alexander Peslyak (public domain).
X11-Basic uses a sha1 algorithm Copyright (C) 2001-2003  Christophe Devine (GPLv2+)


Further notes
=============

The sources for libx11basic and the X11-Basic user manual are maintained in 
the X11-Basic main repository, called "X11Basic" and hosted on gitlab.com. 
They are just cloned into this repository.


Building the .apk file
======================

The build uses the gradle environment. A ./gradlew build should do.

However, I tried to automate also the building of the native libraries from
source. It is still not yet perfect. Maybe you want to send me a patch, if you 
can do better.

X11-Basic needs libx11basic.so (including clapack) and libgmp.so. 

libx11basic.so will completely be build from sources (including clapack).

libgmp.so currently relies on an external git repository which offers 
prebuild libraries for android (which have been build from the 
original GMP sources). The build scrips are there but require a 64-bit linux
environment. So far, I haven't managed to built thouse libraries myself, so a 
little trust in that external project is still required.

Remember cloning this repository with the --recursive option, because it has
submodules:

git clone --recursive git@gitlab.com:kollo/X11-Basic.git

then do a 
cd X11-Basic
./gradlew build
(Enter passwords for the keystore)
(the compile process will take a while, clapack really has many files)

The apk should finally be in build/outputs/apk/X11-Basic-release.apk

If the build script fails, this is usually because the path to the NDK is wrong. 
It is hardcoded in gradle.build and need to be adapted to the location of
your copy of the Android NDK. I have mine in /opt/android-ndk-r7c/

A more detailed compilation tutorial is now included in doc/.

