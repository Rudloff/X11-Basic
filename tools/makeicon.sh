# make the X11-Basic icons from master in correct size and density
# (c) 2013 Markus Hoffmann
#

# convert +antialias -density 100  logo/x11basicVersion.eps -antialias -resize           72x72  src/main/res/drawable/x11basic.png
# convert +antialias -density 75  logo/x11basicVersion.eps -antialias -adaptive-resize  48x48  src/main/res/drawable-mdpi/x11basic.png
convert +antialias -density 50  logo/x11basicVersion.eps -antialias -liquid-rescale   48x48  src/main/res/drawable-mdpi/x11basic.png
convert +antialias -density 75  logo/x11basicVersion.eps -antialias -liquid-rescale   72x72  src/main/res/drawable-hdpi/x11basic.png
convert +antialias -density 110 logo/x11basicVersion.eps -antialias -resize           96x96  src/main/res/drawable-xhdpi/x11basic.png
convert +antialias -density 180 logo/x11basicVersion.eps -antialias -adaptive-resize 144x144 src/main/res/drawable-xxhdpi/x11basic.png
convert  +antialias src/main/res/drawable-xxhdpi/x11basic.png -resize           72x72  src/main/res/drawable/x11basic.png
