' Copy files from X11Basic main repository to X11-Basic for Android repository if necessary.
' (c) Markus Hoffmann
'
'
' make sure, we are in the right directory
a$=DIR$(0)
a$=RIGHT$(a$,LEN(a$)-rinstr(a$,"/"))
IF a$<>"X11-Basic-Android" OR NOT EXIST(".git/HEAD")
  PRINT "error: tool started in the wrong directory."
  QUIT
ENDIF
' see if the main repository is there
IF not EXIST("../X11Basic/.git/HEAD")
  PRINT "error: Main repository is missing."
  QUIT
ENDIF

SYSTEM "rsync -a -v ../X11Basic/RELEASE_NOTES src/main/assets/manual/"
SYSTEM "rsync -a -v ../X11Basic/doc/manual/sec/?.sec src/main/assets/manual/"
SYSTEM "rsync -a -v ../X11Basic/doc/manual/Outputs/* doc/"
SYSTEM "rsync -a -v ../X11Basic/logo/x11basicforAndroid.eps logo/"

t$=" 5x7.c 8x16.c android.c array.c"
t$=t$+" "+"terminal.c wort_sep.c virtual-machine.c xbasic.c"
t$=t$+" "+"variablen.c parser.c number.c parameter.c functions.c"
t$=t$+" "+"decode.c errortxt.c sfunctions.c afunctions.c io.c aes.c"
t$=t$+" "+"svariablen.c do_gets.c window.c file.c ltext.c framebuffer.c"
t$=t$+" "+"bitmap.c graphics.c mathematics.c memory.c runtime.c sysVstuff.c"
t$=t$+" "+"gkommandos.c kommandos.c main.c sound.c"
t$=t$+" "+"consolefont.c bytecode.c xbbc.c fft.c unifont.c"
t$=t$+" "+"unifont57.c raw_mouse.c raw_keyboard.c tools.c io_basic.c loadprg.c"
t$=t$+" "+"type.c lodepng.c spat-a-fnt.c"
t$=t$+" "+"aes.h afunctions.h android.h array.h io.h file.h parameter.h parser.h type.h xbasic.h"
t$=t$+" "+"bitmap.h bytecode.h x11basic.h decode.h functions.h number.h window.h framebuffer.h"
t$=t$+" "+"graphics.h virtual-machine.h wort_sep.h gkommandos.h variablen.h"
t$=t$+" "+"consolefont.h fft.h lodepng.h mathematics.h memory.h raw_mouse.h raw_keyboard.h sound.h"
t$=t$+" "+"kommandos.h parameter.h sfunctions.h sysVstuff.h svariablen.h terminal.h"
t$=t$+" "+"bitmaps"
t$=REPLACE$(t$," "," ../X11Basic/src/")
PRINT t$
SYSTEM "rsync -a -v "+t$+" src/main/jni/"
'print t$
' evtl noch die Beispielprogramme abgleichen....

QUIT
